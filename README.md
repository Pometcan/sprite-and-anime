# Sprite and Anime
---

Bu kütüphane, Love2D ile resim işlemleri ve animasyonları yönetmek için geliştirilmiştir. Şu anda temel olarak resimleri sprite dizilerine dönüştürür ve bu dizileri bir zamanlayıcı kullanarak oynatır.

## Mevcut Özellikler

- Resimleri orantılı bir şekilde kesip dizilere aktarır.
- Bu diziyi bir zamanlayıcıya (timer) atar ve animasyon olarak oynatır.

## Eklenecekler
---
### Sprite
---
- [ ] **Daha Komplike Resim Sprite Oluşturma:**
  - Farklı sprite türlerini destekleyecek şekilde geliştirme.
  - Özelleştirilmiş kesim ve düzenleme seçenekleri.

- [ ] **UI (Kullanıcı Arayüzü):**
  - Sprite düzenleme ve yönetimi için kullanıcı dostu bir arayüz.

- [ ] **Gruplandırma ve İsimlendirme:**
  - Sprite'ları gruplandırma ve her birini anlamlı şekilde isimlendirme.
  - Sprite organizasyonu için etiketleme sistemleri.

- [ ] **Efektler:**
  - **Arka Planı Silme:**
    - Sprite'ların arka planlarını temizlemek için araçlar.
  - **Briç Ayarı (Brightness Adjustment):**
    - Parlaklık seviyelerini ayarlama.
  - **Kontrast Ayarı:**
    - Kontrast seviyelerini düzenleme.
  - **Diğer Efektler:**
    - Gölgeleme, bulanıklık gibi ek görsel efektler.

- [ ] **Diğer Özellikler:**
  - Yüksek çözünürlük desteği.
  - Farklı dosya formatları ile uyum.

### Anime
---
- [ ] **State Machine (Durum Makinesi):**
  - Animasyon durumları arasında geçiş yapabilen bir sistem.
  - Animasyonların farklı durumlar ve olaylarla nasıl yönetileceği.

- [ ] **Detaylı Frame Ayarları:**
  - Frame hızları, geçiş süreleri ve animasyon döngüleri gibi ayrıntıları kontrol etme.
  - Frame bazlı optimizasyonlar.

- [ ] **UI (Kullanıcı Arayüzü):**
  - Animasyon düzenleme ve yönetimi için kullanıcı arayüzü.
  - Animasyon düzenleyicileri ve ön izleme araçları.

- [ ] **Diğer Özellikler:**
  - Animasyonların kolayca senkronize edilmesi.
  - Frame önizleme ve test araçları.
  - Animasyon döngüleri ve geçiş efektleri.

---

Bu kütüphane, oyun ve uygulama geliştirme süreçlerinizde size esneklik ve güç katacak bir dizi özellik sunmayı hedefler. Geliştirme sürecinde bu listeye eklemeler yapılabilir ve yeni özellikler entegre edilebilir.
